$(document).ready(function () {
// //hide sidebar
  $('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
  });

//bellow is the effect on navbar when scrolling
  window.onscroll = function() {myFunction()};
  var navbar = document.getElementById("navbar");
  var target_div = document.getElementById("target_div");
  var sticky = target_div.offsetTop;

  function myFunction() {
    if (window.pageYOffset <= sticky) {
      navbar.classList.add("sticky");
    } else {
      navbar.classList.remove("sticky");
    }
  }

  function readMore() {
    var moreText = document.getElementsByClassName("more");
    var btnText = document.getElementById("myBtn");
    for (var i = 0; i < moreText.length; i++) {
      if (moreText[i].style.display ==='none') {
        btnText.innerHTML = "View less";
        moreText[i].style.display = "inline";
      }else {
        btnText.innerHTML = "View more";
        moreText[i].style.display = 'none';
      }
    }
  }

// //below is the slider of tutor cards
//   $('.post-wrapper').slick({
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     autoplay: true,
//     autoplaySpeed: 2000,
//     nextArrow: $('.nextnext'),
//     prevArrow: $('.previous'),
//   });

  //function to see more cards
//
});
